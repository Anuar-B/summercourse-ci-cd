# Chapter 2: Continuous Deployment

Continuous Deployment means that we are deploying our code as often as possible.
Automatisation is in this case really the way to go, or you can spend a lot of time continuously deploying the code.

For the sake of time we will only be automatically deploying the frontend to an S3 bucket.   

## Step 1: Building our Frontend
To deploy our frontend we are first going to have to build it.
 
The commands to build the frontend are:
```bash
cd frontend
npm install
npm run build
```

### Assignment
Define a job which uses the above CI script to build the frontend. 

## Step 2: Building our Backend
We will also need our backend later on.
 
The commands to build the backend are:
```bash
cd backend
mvn install -B -DskipTests
```

## Step 3: Stages

When using CI, you generally want the result as fast as possible so you have a short feedback loop about the quality of the code.

In the previous steps you added the build steps for frontend and backend. However, we only want to run our build after the tests are successful.

Let's do that! Gitlab supports stages. Let's consider the following yml:

```yml
stages:
- one
- two

job_one_one:
  stage: one
  script: echo "one one"
job_one_two:
  stage: one
  script: echo "one two"
job_two_one:
  stage: two
  script: echo "two one"
job_two_two:
  stage: two
  script: echo "two two"
```

The stages array defines the order in which the stages are executed. In this case the `one` stage is executed first. 
This stage runs `job_one_one` and `job_one_two` in parallel.

After all jobs in the `one` stage succeeds, stage `two` is executed, also containing two jobs in parallel.

### Resources
- https://docs.gitlab.com/ee/ci/yaml/#stages

### Assignment
Apply the above to your configuration; make a `test` phase which runs the tests and a `build` phase for the build steps

## Step 4: Using artifacts
Every pipeline is producing so-called artifacts. This can be:
- Build results (executables, html, js)
- Test Coverage reports
- Information needed in the next step

Most companies have a central artifact repository, like Artifactory or Nexus.
Gitlab has artifact management built-in, so we are going to use that. 

### Resources
- https://docs.gitlab.com/ee/ci/yaml/#artifacts

### Assignment
Consult the above resource to export artifacts of the frontend (`frontend/dist/*`) and backend (`backend/target/*.jar`) and download them to your PC afterwards.


## Step 5: Secrets
We want to deploy our code to AWS S3. But unless we are going to make the S3 Bucket publicly writable (which is very advisable against!) you are going to need credentials in your pipeline.

If you're unfamiliar with AWS, you can set two environment variables so the aws-cli knows how to authenticate.

- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- AWS_DEFAULT_REGION

### Resources
- https://gitlab.com/help/ci/variables/README#variables

### Assignment
Set those variables (ask for the value) using the gitlab Variables section in your CI/CD Settings

## Step 5: Deploying to S3
We can use the artifacts generated in step 2 to deploy them to S3! 
The script to upload the frontend build to S3 looks like this:

```bash
S3_USERNAME="your_username"
aws s3 cp frontend/dist/* s3://summercourse-ci-cd-web-deployment/${S3_USERNAME}/ --recursive
echo "You can find your summercourse website on http://summercourse-ci-cd-web-deployment.s3-website-eu-west-1.amazonaws.com/$S3_USERNAME"
```
For the script to succeed we will first need the artifacts from the build step...

### Resources
- https://docs.gitlab.com/ee/ci/yaml/#dependencies

### Assignment
- Create a job using the above script in a new stage (`deploy`)
- Retrieve the artifact files in the job using the `dependencies` definition. (docs in Resources)
## End of Chapter
You have successfully made it through the second chapter! You can compare your results with the `results/chapter-2` branch now!
